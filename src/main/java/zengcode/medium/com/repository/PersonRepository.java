package zengcode.medium.com.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import zengcode.medium.com.model.Person;


public interface PersonRepository extends GraphRepository<Person> {

    Person findByName(String name);
}
