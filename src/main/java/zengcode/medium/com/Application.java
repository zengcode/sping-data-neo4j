package zengcode.medium.com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import zengcode.medium.com.model.Person;
import zengcode.medium.com.repository.PersonRepository;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableNeo4jRepositories
public class Application {

    private final static Logger log = LoggerFactory.getLogger(Application.class);


    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        Application application = ctx.getBean(Application.class);
    }

    @Bean
    CommandLineRunner demo(PersonRepository personRepository) {
        return args -> {

            personRepository.deleteAll();

            Person pea = new Person("Pea");
            Person bee = new Person("Bee");
            Person nee = new Person("Nee");
            Person pang = new Person("Pang");
            Person ni = new Person("Ni");


            List<Person> friends = Arrays.asList(pea, bee, nee, pang);

            log.info("Before linking friend with Neo4j...");

            friends.stream()
                    .forEach(person -> log.info("\t" + person.toString()));

            personRepository.save(pea);
            personRepository.save(bee);
            personRepository.save(nee);
            personRepository.save(pang);
            personRepository.save(ni);

            pea = personRepository.findByName(pea.getName());
            pea.friendOf(bee);
            pea.friendOf(nee);
            personRepository.save(pea);

            bee = personRepository.findByName(bee.getName());
            bee.friendOf(nee);

            personRepository.save(bee);

            pang = personRepository.findByName(pang.getName());
            pang.friendOf(nee);
            personRepository.save(pang);

            ni = personRepository.findByName(ni.getName());
            ni.friendOf(pea);
            personRepository.save(ni);

            log.info("Lookup each person ...");
            friends.stream().forEach(person -> log.info(
                    "\t" + personRepository.findByName(person.getName()).toString()));


            log.info("Looking up for Pea's friends");
            pea.getFriends()
                    .forEach(friend -> {
                        log.info("\t" + friend.getName());
                    });
        };
    }


}
